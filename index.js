const fs = require('fs');
const os = require('os');
const http = require('http');
const PORT = 3000;

Input();

// User menu
function Menu(){
    fs.readFile('menu.txt', 'utf-8', function(err, file){
    if (err) throw err;
    console.log(file);
    process.stdout.write('Type a number: ');
    });
}

// Get user input
function Input(){
    Menu();
    let input = process.stdin;
    input.on('data', function(data){
        console.log('You chose: ' + data);
        if(data == 1){
            PrintJson();
        } else if(data == 2){
            DisplayOSInfo();
        } else if(data == 3){
            StartHTTPServer();
        } else if(data ==4){
            console.log('Exiting...');
            process.exit();
        } else {
            console.log('Invalid input');
        }
        // While listener (input.on) is open, show menu again.
        setTimeout(Menu, 2000);
    });
}

// Print package.json
function PrintJson(){
    console.log('Parsing JSON file...');
    let rawdata = fs.readFileSync('package.json');
    let package = JSON.parse(rawdata);
    console.log(package);
}

// Display OS info
function DisplayOSInfo(){
    console.log('Getting OS info...');
    console.log('SYSTEM MEMORY: ', (os.totalmem() / 1024 / 1024 / 1024).toFixed(2) + ' GB');
    console.log('FREE MEMORY: ', (os.freemem() / 1024 / 1024 / 1024).toFixed(2) + ' GB');
    console.log('CPU CORES: ', os.cpus().length);
    console.log('ARCHITECTURE: ', os.arch());
    console.log('PLATFORM: ', os.platform());
    console.log('RELEASE: ', os.release());
    console.log('USER: ', os.userInfo().username);
}

// Start HTTP server
function StartHTTPServer(){
    console.log('Starting HTTP server...');
    const server = http.createServer((req, res) => {
        if (req.url === '/'){
            res.write('Hello, world!');
            res.end();
        }
    });
    server.listen(PORT);
    console.log('Listening at port ' + PORT);
}